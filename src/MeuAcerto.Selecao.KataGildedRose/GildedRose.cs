﻿using System.Collections.Generic;

namespace MeuAcerto.Selecao.KataGildedRose
{
    class GildedRose
    {
        IList<Item> Itens;
        public GildedRose(IList<Item> Itens)
        {
            this.Itens = Itens;
        }

        public void AtualizarQualidade()
        {
            foreach (Item item in Itens)
            {
                if (item.Nome != "Queijo Brie Envelhecido" && item.Nome != "Ingressos para o concerto do Turisas")
                {
                    item.Qualidade -= item.Qualidade > 0 && item.Nome != "Dente do Tarrasque"? 1 : 0;
                    item.Qualidade -= item.Qualidade > 0 && item.Nome.IndexOf("Conjurado") != -1 ? 1 :0;
                }
                else
                {
                    if (item.Qualidade < 50)
                    {
                        item.Qualidade += 1;

                        if (item.Nome == "Ingressos para o concerto do Turisas")
                        {
                            item.Qualidade += item.PrazoParaVenda < 11 && item.Qualidade < 50 ?  1 : 0;
                            item.Qualidade += item.PrazoParaVenda < 6 && item.Qualidade < 50 ? 1 :0 ;
                        }
                    }
                }
                item.PrazoParaVenda -= item.Nome != "Dente do Tarrasque" ? 1 : 0;
                if (item.PrazoParaVenda < 0)
                {
                    if (item.Nome != "Queijo Brie Envelhecido")
                    {
                        if (item.Nome != "Ingressos para o concerto do Turisas")
                        {
                            item.Qualidade -= item.Qualidade > 0 && item.Nome != "Dente do Tarrasque" ? 1 : 0;      
                            item.Qualidade -= item.Qualidade > 0 && item.Nome.IndexOf("Conjurado") != -1 ? 1 :0;                 
                         }
                        else
                        {
                            item.Qualidade = 0;
                        }
                    }
                    else
                    {
                        item.Qualidade += item.Qualidade < 50 ? 1 : 0;
                    }
                }
            }
        }
    }
}
